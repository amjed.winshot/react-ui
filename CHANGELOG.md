# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.7.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.6...v1.7.0) (2024-03-22)


### Features

* handle invalid token ([71499aa](https://gitlab.com/amjed.winshot/react-ui/commit/71499aae0cfbbcd99369b903f3ff50344abe51a9))

### [1.6.6](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.5...v1.6.6) (2024-03-22)

### [1.6.5](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.4...v1.6.5) (2024-03-21)

### [1.6.4](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.3...v1.6.4) (2024-03-21)

### [1.6.3](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.2...v1.6.3) (2024-03-21)

### [1.6.2](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.1...v1.6.2) (2024-03-21)


### Bug Fixes

* **end-points:** change end points methods ([6b604d3](https://gitlab.com/amjed.winshot/react-ui/commit/6b604d3f32e7a0957b4009c8210fe49477828b97))

### [1.6.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.6.0...v1.6.1) (2024-03-21)

## [1.6.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.5.0...v1.6.0) (2024-03-21)

## [1.5.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.2-beta.2...v1.5.0) (2024-03-19)


### Features

* add get Current User ([9ed1ced](https://gitlab.com/amjed.winshot/react-ui/commit/9ed1cedd34e2d0e91c743e4f21e06c3e261b40f3))
* add isTokenResponseError function to test agains errors ([1154bd8](https://gitlab.com/amjed.winshot/react-ui/commit/1154bd848734aeac7c9643097cc9f71a834220f7))

## [1.5.0-next.4](https://gitlab.com/amjed.winshot/react-ui/compare/v1.5.0-next.3...v1.5.0-next.4) (2024-03-13)

## [1.5.0-next.3](https://gitlab.com/amjed.winshot/react-ui/compare/v1.5.0-next.2...v1.5.0-next.3) (2024-03-12)


### Features

* add isTokenResponseError function to test agains errors ([1154bd8](https://gitlab.com/amjed.winshot/react-ui/commit/1154bd848734aeac7c9643097cc9f71a834220f7))

## [1.5.0-next.2](https://gitlab.com/amjed.winshot/react-ui/compare/v1.5.0-next.1...v1.5.0-next.2) (2024-03-12)


### Features

* add onTokenExpired function ([8918e2a](https://gitlab.com/amjed.winshot/react-ui/commit/8918e2a873ad14b177a61a26a45c4cfc798c083b))

## [1.5.0-next.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.5.0-next.0...v1.5.0-next.1) (2024-03-12)


### Features

* add onRefreshTokenExpired function ([b348436](https://gitlab.com/amjed.winshot/react-ui/commit/b348436f98e66c7e710ee76f23913270b1011a91))

## [1.5.0-next.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.2-beta.2...v1.5.0-next.0) (2024-03-11)


### Features

* add get Current User ([9ed1ced](https://gitlab.com/amjed.winshot/react-ui/commit/9ed1cedd34e2d0e91c743e4f21e06c3e261b40f3))

### [1.4.2-beta.2](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.2-beta.1...v1.4.2-beta.2) (2024-03-06)

### [1.4.2-beta.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.2-beta.0...v1.4.2-beta.1) (2024-03-06)

### [1.4.2-beta.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.2-alpha.0...v1.4.2-beta.0) (2024-03-06)

### [1.4.2-alpha.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.1...v1.4.2-alpha.0) (2024-03-05)

### [1.4.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.4.0...v1.4.1) (2024-01-24)


### Bug Fixes

* replace token_type_hint with grant_type ([140fdbe](https://gitlab.com/amjed.winshot/react-ui/commit/140fdbe81abb30cef1fb2556f411d1caa7583adc))

## [1.4.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.3.0...v1.4.0) (2023-12-28)


### Features

* add errors helper ([8ba0f45](https://gitlab.com/amjed.winshot/react-ui/commit/8ba0f459a922da7c9b9034034d3406bfba8c4854))

## [1.3.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.2.3...v1.3.0) (2023-12-28)


### Features

* handle refresh token ([f5ee6bc](https://gitlab.com/amjed.winshot/react-ui/commit/f5ee6bcf1256cfc4ac07919197f3b723ed0369fc))

### [1.2.3](https://gitlab.com/amjed.winshot/react-ui/compare/v1.2.2...v1.2.3) (2023-12-28)

### [1.2.2](https://gitlab.com/amjed.winshot/react-ui/compare/v1.2.1...v1.2.2) (2023-12-28)


### Bug Fixes

* make axios instance public ([c9f2407](https://gitlab.com/amjed.winshot/react-ui/commit/c9f240714d5901bf6ec66fd20d61c73fe40bb0b6))

### [1.2.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.2.0...v1.2.1) (2023-12-28)

## [1.2.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.1.1...v1.2.0) (2023-12-27)

### [1.1.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.1.0...v1.1.1) (2023-12-27)

## [1.1.0](https://gitlab.com/amjed.winshot/react-ui/compare/v1.0.4...v1.1.0) (2023-12-18)


### Features

* add accessTokenEventListener ([0ad1196](https://gitlab.com/amjed.winshot/react-ui/commit/0ad11960daf66d2aa8ad7b6c2eddec5b2e58653d))

### [1.0.4](https://gitlab.com/amjed.winshot/react-ui/compare/v1.0.3...v1.0.4) (2023-12-15)

### [1.0.3](https://gitlab.com/amjed.winshot/react-ui/compare/v1.0.2...v1.0.3) (2023-12-15)

### [1.0.2](https://gitlab.com/amjed.winshot/react-ui/compare/v1.0.1...v1.0.2) (2023-12-15)

### [1.0.1](https://gitlab.com/amjed.winshot/react-ui/compare/v1.0.0...v1.0.1) (2023-12-15)


### Bug Fixes

* update revoke endpoint ([69c5719](https://gitlab.com/amjed.winshot/react-ui/commit/69c571944826f70cd0c3be6362f609c934f013d1))

## [1.0.0](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.7...v1.0.0) (2023-12-15)


### Bug Fixes

* fix token revocation ([25b48b9](https://gitlab.com/amjed.winshot/react-ui/commit/25b48b921fc25e78b230b3ee6c0ec41f5d7b720f))
* make sure the access token is send in revocation method ([635dc7f](https://gitlab.com/amjed.winshot/react-ui/commit/635dc7f72eb32f1c65efda90f077d36ee5fdeb4e))

### [0.0.7](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.6...v0.0.7) (2023-12-13)

### [0.0.6](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.5...v0.0.6) (2023-11-20)


### Features

* add suport for types ([1e95248](https://gitlab.com/amjed.winshot/react-ui/commit/1e9524862db6e270ce7779e7f88aee7a3b64796b))

### [0.0.5](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.4...v0.0.5) (2023-11-20)

### [0.0.4](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.3...v0.0.4) (2023-11-20)


### Features

* manage access token ([b7fb218](https://gitlab.com/amjed.winshot/react-ui/commit/b7fb2186e79459f1b7808c62fe95a4d478b17cf0))

### [0.0.3](https://gitlab.com/amjed.winshot/react-ui/compare/v0.0.2...v0.0.3) (2023-11-16)

### 0.0.2 (2023-11-15)


### Bug Fixes

* standard-version re-install ([ca27c7c](https://gitlab.com/amjed.winshot/react-ui/commit/ca27c7c8ee89be1a4079d33a57b78ce40c1fb0f5))
