import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import { IdentityProvider } from '../lib/IdentityProvider';
import { TokenResponse } from '../lib/types/index.ts';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <IdentityProvider
    storagePersister={{
      getTokenResponse() {
        const tokenResponse = localStorage.getItem('t');
        if (tokenResponse) {
          return JSON.parse(tokenResponse) as TokenResponse;
        }
        return null;
      },
      removeTokenResponse() {
        return localStorage.removeItem('t');
      },
      saveTokenResponse(tokenResponse) {
        localStorage.setItem('t', JSON.stringify(tokenResponse));
      },
    }}
    IPD_URL={'http://192.168.1.14:3008'}
    clientId={'winshot-web-app'}
  >
    <App />
  </IdentityProvider>,
);
