import { useEffect, useState } from 'react';
import { useIdentityProvider } from '../lib/main';

function App() {
  const idpFuncs = useIdentityProvider();
  const [inputValue, setInputValue] = useState('');
  const [emailValue, setEmailValue] = useState('');
  const [emailUser, setEmailUser] = useState('');
  useEffect(() => {}, []);
  const requestOtp = async () => {
    try {
      const res = await idpFuncs.requestOtp({ username: emailValue });
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  };

  const loginWithOtp = async () => {
    try {
      const res = await idpFuncs.loginWithOtp({ username: emailValue, code: inputValue });
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.name === 'code') {
      setInputValue(event.target.value);
    } else if (event.target.name === 'email') {
      setEmailValue(event.target.value);
    }
  };

  const getCUrrentUser = async () => {
    try {
      const user = idpFuncs.currentUser;
      // console.log(user);
      // console.log({ loggedIn: idpFuncs.isLoggedIn });
      setEmailUser(user?.email ?? '');
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <input type="text" name="email" onChange={handleInputChange} />
      <br />

      <br />
      <button onClick={requestOtp}>Request Otp</button>
      <br />

      <br />
      <input type="text" name="code" onChange={handleInputChange} />
      <h1>test </h1>
      <button onClick={loginWithOtp}>Login With Otp</button>
      <h1> test currentUser</h1>
      <button onClick={getCUrrentUser}>CurrentUser</button>
      <h2>email of user is : {emailUser}</h2>
    </div>
  );
}

export default App;
