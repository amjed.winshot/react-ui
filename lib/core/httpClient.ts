import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import {
  LoginWithOtpArgs,
  LoginWithUserNameAndPasswordArgs,
  TokenResponse,
  SyncStoragePersister,
  CurrentUser,
  ChangePasswordArgs,
  CheckVerificationCodeResponsType,
  VerifyUserType,
  ResetPasswordType,
  ResetPasswordResponseType,
} from '../types';
import { IdentityProviderProps } from '../types/index';
declare module 'axios' {
  export interface AxiosInstance {
    request<T = object>(config: AxiosRequestConfig): Promise<T>;
    get<T = object>(url: string, config?: AxiosRequestConfig): Promise<T>;
    delete<T = object>(url: string, config?: AxiosRequestConfig): Promise<T>;
    head<T = object>(url: string, config?: AxiosRequestConfig): Promise<T>;
    post<T = object>(url: string, data?: object, config?: AxiosRequestConfig): Promise<T>;
    put<T = object>(url: string, data?: object, config?: AxiosRequestConfig): Promise<T>;
    patch<T = object>(url: string, data?: object, config?: AxiosRequestConfig): Promise<T>;
  }
  export interface InternalAxiosRequestConfig {
    _retry?: boolean;
  }
}

export class HttpClient {
  instance: AxiosInstance;
  clientId?: string;
  storagePersister?: SyncStoragePersister;
  private transformHeaders?: IdentityProviderProps['transformHeaders'];
  constructor() {
    this.instance = axios.create();

    this.instance.interceptors.request.use((config) => {
      const tokenResponse = this.storagePersister ? this.storagePersister.getTokenResponse() : null;
      if (typeof this.transformHeaders === 'function') {
        config.headers = this.transformHeaders(config.headers);
      }

      if (tokenResponse) {
        config.headers.Authorization = `Bearer ${tokenResponse.access_token}`;
      }
      return config;
    });
    this.instance.interceptors.response.use(this._handleResponse);
  }
  setBASE_URL(baseUrl: string) {
    this.instance.defaults.baseURL = baseUrl;
    return this;
  }
  setClientId(clientId: string) {
    this.clientId = clientId;
    return this;
  }
  setStoragePersister(storagePersister: SyncStoragePersister) {
    this.storagePersister = storagePersister;
    return this;
  }
  setTransformHeaders(transformHeaders: IdentityProviderProps['transformHeaders']) {
    this.transformHeaders = transformHeaders;
    return this;
  }

  private _handleResponse = (response: AxiosResponse) => response.data;

  requestNewAccessToken = (refresh_token: string) => {
    return new Promise<TokenResponse>((resolve, reject) => {
      this.instance
        .post<TokenResponse>('/oauth2/token', {
          grant_type: 'refresh_token',
          refresh_token: refresh_token,
          client_id: this.clientId,
        })
        .then((response) => {
          this.storagePersister!.saveTokenResponse(response);
          resolve(response);
        })
        .catch(reject);
    });
  };

  login = <T extends LoginWithUserNameAndPasswordArgs | LoginWithOtpArgs>(payload: T) =>
    this.instance.post<TokenResponse>('/oauth2/token', {
      ...payload,
      client_id: this.clientId,
    });

  requestOtp = (payload: Omit<LoginWithOtpArgs, 'code'>) =>
    this.instance.post<TokenResponse>('/oauth2/requestOtp', {
      ...payload,
      client_id: this.clientId,
    });
  changePassword = (body: ChangePasswordArgs) => this.instance.put(`/auth/password`, body);
  checkVerificationCode = (verificationCode: string) =>
    this.instance.get<CheckVerificationCodeResponsType>(`/auth/check-verification-code/${verificationCode}`);
  getCurrentUser = () => this.instance.get<CurrentUser>('/oauth2/whoami');

  resetPassword = (body: ResetPasswordType) =>
    this.instance.post<ResetPasswordResponseType>('/auth/resetPassword', { ...body, client_id: this.clientId });
  verifyUser = (body: VerifyUserType) => this.instance.post<void>(`/auth/verify-user`, body);
  confirmResetPassword = (body: VerifyUserType) => this.instance.post<void>('auth/ConfirmResetPassword', body);

  isTokenExpired = (error: AxiosError<{ reason?: string }>) => {
    const originalRequest = error.config!;
    return error.response && error.response.status === 401 && error.response.data?.reason === 'TokenExpired' && !originalRequest._retry;
  };

  revoke = (fallbackAccessToken?: string) => {
    let finalAccessToken = fallbackAccessToken;

    if (!finalAccessToken) {
      const tokenResponse = this.storagePersister ? this.storagePersister.getTokenResponse() : null;
      if (tokenResponse) {
        finalAccessToken = tokenResponse.access_token;
      }
    }

    if (!finalAccessToken) {
      return Promise.resolve();
    }

    return this.instance.post(
      '/oauth2/revoke',
      {
        token: finalAccessToken,
        token_type_hint: 'password',
      },
      {
        headers: {
          Authorization: `Bearer ${finalAccessToken}`,
        },
      },
    );
  };
}

export const _nativeHttpClient = new HttpClient();
