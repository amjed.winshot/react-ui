import axios, { AxiosError, AxiosInstance, AxiosResponse, InternalAxiosRequestConfig, isAxiosError } from 'axios';
import { _nativeHttpClient } from './httpClient';
import createAuthRefreshInterceptor from 'axios-auth-refresh';

interface HttpClientInterface {
  isTokenExpired(error: AxiosError<{ reason?: string }>): boolean;
  handleRequest(config: InternalAxiosRequestConfig<any>): InternalAxiosRequestConfig<any>;
  handleResponse(response: AxiosResponse): any;
}
type HttpClientAbstractOptions = {
  onRefreshTokenExpired?: () => void;
  onTokenExpired?: () => void;
};

export abstract class HttpClientAbstract implements HttpClientInterface {
  public readonly instance: AxiosInstance;
  private options: HttpClientAbstractOptions;
  constructor(BASE_URL: string, options: HttpClientAbstractOptions = {}) {
    this.options = options;

    this.instance = axios.create({
      baseURL: BASE_URL,
    });

    this.instance.interceptors.request.use((props) => this.handleRequest(props));
    this.instance.interceptors.response.use((e) => this.handleResponse(e));

    // Instantiate the interceptor
    createAuthRefreshInterceptor(this.instance, (error) => this.refreshAuthLogic(error), {
      shouldRefresh: (err) => this.isTokenExpired(err as AxiosError<{ reason?: string | undefined }, any>),
      pauseInstanceWhileRefreshing: true,
    });
  }

  refreshAuthLogic = (failedRequest: AxiosError) => {
    if (this.options?.onTokenExpired) {
      this.options.onTokenExpired();
    }
    const accessTokenResponse = _nativeHttpClient.storagePersister!.getTokenResponse();

    if (!accessTokenResponse) {
      return Promise.reject(failedRequest);
    }
    const result = _nativeHttpClient
      .requestNewAccessToken(accessTokenResponse.refresh_token)
      .then((newAccessToken) => {
        _nativeHttpClient.storagePersister?.saveTokenResponse(newAccessToken);
        failedRequest.response!.config.headers['Authorization'] = newAccessToken.access_token;
        return Promise.resolve();
      })
      .catch((newError) => {
        if (isAxiosError(newError)) {
          if (newError.response && (newError.response.status === 401 || newError.response.status === 400)) {
            _nativeHttpClient.storagePersister?.removeTokenResponse();
            const errorWithReason: AxiosError<any> = {
              ...failedRequest,
            };
            errorWithReason.response!.data = {
              reason: 'RefreshTokenExpired',
            };

            if (this.options?.onRefreshTokenExpired) {
              this.options.onRefreshTokenExpired();
            }

            return Promise.reject(errorWithReason);
          }
        }
        return Promise.reject(failedRequest);
      });
    return result;
  };
  abstract handleRequest(config: InternalAxiosRequestConfig<any>): InternalAxiosRequestConfig<any>;

  abstract handleResponse(response: AxiosResponse<any, any>): any;

  isTokenExpired = (error: AxiosError<{ reason?: string | undefined }, any>) => {
    if (error.response?.status === 401) {
      return error.response.data?.reason === 'TokenExpired';
    }
    return false;
  };
}
