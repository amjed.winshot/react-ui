import { AxiosRequestHeaders } from 'axios';
import React from 'react';
type fn = (accessToken: string | undefined) => void;
type ListenerFn = (listener: fn) => (() => void) | undefined;
export type IdentityContextProps = {
  requestOtp: (args: Omit<LoginWithOtpArgs, 'code'>) => Promise<boolean | TokenResponseError>;
  loginWithOtp: (args: LoginWithOtpArgs) => Promise<TokenResponse | TokenResponseError>;
  isLoggedIn: boolean;
  loginWithUserNameAndPassword: (args: LoginWithUserNameAndPasswordArgs) => Promise<TokenResponse | TokenResponseError>;
  /**
   *
   * @param fallbackAccessToken used in the `Authorization` header as "Bearer `token`".
   * If not provided, We'll try to get it from `storagePersister` if it exists. if not exists, we'll not send the request.
   *
   */
  logout: (fallbackAccessToken?: string) => void;
  currentUser: CurrentUser | undefined;
  changePassword: (args: ChangePasswordArgs) => Promise<unknown | TokenResponseError>;
  checkVerificationCode: (args: { verificationCode: string }) => Promise<CheckVerificationCodeResponsType | TokenResponseError>;
  resetPassword: (args: ResetPasswordType) => Promise<ResetPasswordResponseType | TokenResponseError>;
  verifyUser: (args: VerifyUserType) => Promise<void | TokenResponseError>;
  confirmResetPassword: (args: VerifyUserType) => Promise<void | TokenResponseError>;
};

export interface IdentityProviderProps {
  children: React.ReactNode;
  /* The `clientId: string;` property in the `IdentityProviderProps` interface is used to specify the
  client ID that will be used for authentication and authorization with the Identity Provider (IPD).
  The client ID is a unique identifier assigned to the client application by the IPD, and it is used
  to identify the client application when making requests to the IPD for authentication and
  authorization purposes. */
  clientId: string;

  /* The `IPD_URL: string;` is a property in the `IdentityProviderProps` interface. It is used to
  specify the URL of the Identity Provider (IPD) that will be used for authentication and
  authorization. The value of `IPD_URL` should be a string representing the URL of the IPD. */
  IPD_URL: string;

  storagePersister: SyncStoragePersister;

  accessTokenEventListener?: ListenerFn;

  transformHeaders?: (headers: AxiosRequestHeaders) => AxiosRequestHeaders;
}
export type SyncStoragePersister = {
  saveTokenResponse: (tokenResponse: TokenResponse) => void;
  getTokenResponse: () => TokenResponse | null;
  removeTokenResponse: () => void;
};

export type TokenResponse = {
  access_token: string;
  refresh_token: string;
  token_type: 'Bearer';
  expires_in: number;
  scope: string;
};

export type TokenResponseError = {
  error: Error;
  /**
   *
   * UNAUTHORIZED: username or password is wrong or user is not verified
   *
   * FORBIDDEN: user is not allowed to login to this application
   *
   * UNKNOWN: unknown error
   *
   * BAD_REQUEST: bad request
   * CLIENT_NOT_FOUND: client not found with status code 404
   */
  reason: 'UNAUTHORIZED' | 'FORBIDDEN' | 'UNKNOWN' | 'BAD_REQUEST' | 'CLIENT_NOT_FOUND' | 'NOT_FOUND' | 'USER_NOT_FOUND';
};

export type LoginWithUserNameAndPasswordArgs = {
  username: string;
  password: string;
};

export type LoginWithOtpArgs = {
  username: string;
  code: string;
};

export type ChangePasswordArgs = {
  oldPassword: string;
  newPassword: string;
};

export type CurrentUser = {
  _id: string;
  username?: string;
  fullName: string;
  email?: string;
  phoneNumber?: string;
  roles: string[];
  isVerified: boolean;
  createdAt: Date;
  updatedAt: Date;
};

export type CheckVerificationCodeResponsType = Pick<CurrentUser, '_id' | 'username' | 'fullName' | 'email' | 'roles'> & {
  verificationCode: string;
};

export type ResetPasswordType = Pick<CurrentUser, 'username'>;
export type ResetPasswordResponseType = Pick<CurrentUser, 'email' | 'phoneNumber'>;

export type VerifyUserType = {
  verificationCode: string;
  password: string;
};
