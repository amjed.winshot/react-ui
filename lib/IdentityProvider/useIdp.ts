import { isAxiosError } from 'axios';
import { useEffect, useRef, useState } from 'react';
import {
  ChangePasswordArgs,
  CheckVerificationCodeResponsType,
  CurrentUser,
  IdentityContextProps,
  IdentityProviderProps,
  LoginWithOtpArgs,
  LoginWithUserNameAndPasswordArgs,
  ResetPasswordResponseType,
  ResetPasswordType,
  TokenResponse,
  TokenResponseError,
  VerifyUserType,
} from '../types';

import { _nativeHttpClient } from '../core';
import { handleError } from './utils';
import { isTokenInvalid } from '../utils/errorHelper';

type UseIdpProps = Omit<IdentityProviderProps, 'children'>;

function useIdp({ IPD_URL, clientId, storagePersister, transformHeaders, accessTokenEventListener }: UseIdpProps): IdentityContextProps {
  /**
   *
   */
  const httpClient = useRef(
    _nativeHttpClient.setBASE_URL(IPD_URL).setClientId(clientId).setStoragePersister(storagePersister).setTransformHeaders(transformHeaders),
  ).current;
  const [currentUser, setCurrentUser] = useState<CurrentUser | undefined>();
  const [isLoggedIn, setIsLoggedIn] = useState(() => {
    const accessToken = storagePersister.getTokenResponse();
    return accessToken !== null;
  });

  useEffect(() => {
    const shouldListen = typeof accessTokenEventListener === 'function';
    let unsubscribe: (() => void) | undefined;
    if (shouldListen) {
      unsubscribe = accessTokenEventListener((newAccessToken) => {
        if (newAccessToken) {
          setIsLoggedIn(true);
        } else {
          setIsLoggedIn(false);
        }
      });
    }
    return () => {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, []);
  useEffect(() => {
    async function fetchCurrentUser() {
      httpClient
        .getCurrentUser()
        .then(setCurrentUser)
        .catch((error) => {
          if (isTokenInvalid(error)) {
            // should logout
            storagePersister.removeTokenResponse();
            setIsLoggedIn(false);
          }
        });
    }
    if (isLoggedIn) {
      fetchCurrentUser();
    }
  }, [isLoggedIn]);

  /**
   * The function `loginWithUserNameAndPassword` is used to authenticate a user with their username and
   * password and return a token response or an error.
   * @param {LoginWithUserNameAndPasswordArgs}  - The `loginWithUserNameAndPassword` function takes an
   * object as an argument with the following properties:
   * @returns The function `loginWithUserNameAndPassword` returns a Promise that resolves to either a
   * `TokenResponse` object or a `TokenResponseError` object.
   */

  function loginWithUserNameAndPassword({ username, password }: LoginWithUserNameAndPasswordArgs) {
    return new Promise<TokenResponse | TokenResponseError>((resolve, reject) => {
      const payload = {
        grant_type: 'password',
        username,
        password,
        client_id: clientId,
      };
      httpClient
        .login(payload)
        .then((response) => {
          storagePersister.saveTokenResponse(response);
          setIsLoggedIn(true);
          resolve(response);
        })
        .catch((error) => {
          const rejection: TokenResponseError = {
            error,
            reason: 'UNKNOWN',
          };

          if (isAxiosError(error)) {
            if (error.response?.status === 401) {
              rejection.reason = 'FORBIDDEN';
            } else if (error.response?.status === 400) {
              rejection.reason = 'BAD_REQUEST';
            } else if (error.response?.status === 403) {
              rejection.reason = 'FORBIDDEN';
            } else if (error.response?.status === 404) {
              rejection.reason = 'CLIENT_NOT_FOUND';
            }
          }
          reject(rejection);
        });
    });
  }
  /**
   *
   * @param fallbackAccessToken used in the `Authorization` header as "Bearer `token`".
   * If not provided, We'll try to get it from `storagePersister` if it exists. if not exists, we'll not send the request.
   *
   */
  function logout(fallbackAccessToken?: string) {
    httpClient.revoke(fallbackAccessToken);
    storagePersister.removeTokenResponse();
    setIsLoggedIn(false);
  }

  /**
   * The function `loginWithOtp` is used to authenticate a user with their username and
   * code and return a token response or an error.
   * @param {LoginWithOtpArgs}  - The 'loginWithOtp' function takes an object as an argument with
   * with the following properties:
   * @returns The function 'loginWithOtp' return a Promise that resolves to either a 'TokenResponse'
   * object or a 'TokenResponseError' object.
   */

  function loginWithOtp({ username, code }: LoginWithOtpArgs) {
    return new Promise<TokenResponse | TokenResponseError>((resolve, reject) => {
      const payload = {
        grant_type: 'otp',
        username,
        code,
      };

      httpClient
        .login(payload)
        .then((response) => {
          storagePersister.saveTokenResponse(response);
          setIsLoggedIn(true);
          resolve(response);
        })
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }

  /**
   * The function `requestToOtp` is used to request an OTP to a user with their username and client_id,
   * and return a true or an error.
   * @param {requestToOtp}  - The 'requestToOtp' function takes an object as an argument with the
   * fillowing properties:
   * @returns The function 'requestToOtp' returns a Promise that resolves to either a 'true' or a
   * 'TokenResponseError' object.
   */
  function requestOtp({ username }: Omit<LoginWithOtpArgs, 'code'>) {
    return new Promise<true | TokenResponseError>((resolve, reject) => {
      const payload = {
        grant_type: 'otp',
        username,
      };
      httpClient
        .requestOtp(payload)
        .then(() => {
          resolve(true);
        })
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }

  function changePassword({ oldPassword, newPassword }: ChangePasswordArgs) {
    return new Promise<unknown | TokenResponseError>((resolve, reject) => {
      httpClient
        .changePassword({ oldPassword, newPassword })
        .then(resolve)
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }

  function resetPassword(body: ResetPasswordType) {
    return new Promise<ResetPasswordResponseType | TokenResponseError>((resolve, reject) => {
      httpClient
        .resetPassword(body)
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          const rejection = handleError(error);
          if (isAxiosError(error)) {
            if (error.response?.status === 404) {
              rejection.reason = 'USER_NOT_FOUND';
            }
          }
          reject(rejection);
        });
    });
  }
  function confirmResetPassword(data: VerifyUserType) {
    return new Promise<void | TokenResponseError>((resolve, reject) => {
      httpClient
        .confirmResetPassword(data)
        .then(() => {
          resolve();
        })
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }
  function verifyUser(data: VerifyUserType) {
    return new Promise<void | TokenResponseError>((resolve, reject) => {
      httpClient
        .verifyUser(data)
        .then(() => {
          resolve();
        })
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }
  function checkVerificationCode({ verificationCode }: { verificationCode: string }) {
    return new Promise<CheckVerificationCodeResponsType | TokenResponseError>((resolve, reject) => {
      httpClient
        .checkVerificationCode(verificationCode)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          const rejection = handleError(error);
          reject(rejection);
        });
    });
  }

  return {
    requestOtp,
    loginWithOtp,
    isLoggedIn,
    loginWithUserNameAndPassword,
    logout,
    currentUser,
    changePassword,
    checkVerificationCode,
    resetPassword,
    verifyUser,
    confirmResetPassword,
  };
}

export default useIdp;
