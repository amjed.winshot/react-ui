import { isAxiosError } from 'axios';
import { TokenResponseError } from '../types';
export function handleError(error: any) {
  const rejection: TokenResponseError = {
    error,
    reason: 'UNKNOWN',
  };
  if (isAxiosError(error)) {
    if (error.response?.status === 401) {
      rejection.reason = 'UNAUTHORIZED';
    } else if (error.response?.status === 400) {
      rejection.reason = 'BAD_REQUEST';
    } else if (error.response?.status === 403) {
      rejection.reason = 'FORBIDDEN';
    } else if (error.response?.status === 404) {
      rejection.reason = 'NOT_FOUND';
    }
  }
  return rejection;
}
