import React from 'react';
import { IdentityContextProps } from '../types';

export const IdentityContext = React.createContext({} as IdentityContextProps);
