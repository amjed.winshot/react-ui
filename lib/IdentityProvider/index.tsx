import React from 'react';
import { IdentityProviderProps } from '../types';
import { IdentityContext } from './IdentityContext';
import useIdp from './useIdp';

export function IdentityProvider({ children, ...rest }: IdentityProviderProps) {
  const props = useIdp(rest);
  return <IdentityContext.Provider value={props}>{children}</IdentityContext.Provider>;
}
