import { isAxiosError } from 'axios';
import { TokenResponseError } from '../types';

export function isTokenExpired(error: unknown) {
  if (isAxiosError(error) && error.response?.status === 401) {
    return error.response.data?.reason === 'TokenExpired';
  }
  return false;
}
export function isTokenInvalid(error: unknown) {
  if (isAxiosError(error) && error.response?.status === 401 && !isTokenExpired(error)) {
    return true;
  }
  return false;
}
export function isRefreshTokenExpired(error: unknown) {
  if (isAxiosError(error) && (error.response?.status === 401 || error.response?.status === 400)) {
    return error.response.data?.reason === 'RefreshTokenExpired';
  }
  return false;
}
function isObject(thing: any): thing is object {
  return typeof thing === 'object' && thing !== null;
}

export function isIDPError(error: unknown): error is TokenResponseError {
  if (!isObject(error)) {
    return false;
  }
  if ('reason' in error && 'error' in error && isAxiosError(error.error)) {
    return true;
  }
  return false;
}

/**
 *
 * @deprecated use `isIDPError` instead
 */
export const isTokenResponseError = isIDPError;
