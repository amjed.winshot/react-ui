### Gofield React UI

Unified typescript SDK library that contains All React UI library for & IDP.

### Installation

1. install command

```bash
# using yarn
yarn add @gofiled/react-ui

# using npm
npm install @gofiled/react-ui

```

### Usage

#### IdentityProvider: Component

```javascript
import { IDP } from '@gofiled/react-ui';

// Wrap you app with "IdentityProvider" component
```

#### Properties IdentityProvider

| Property           | Description                                                                          | Default Value |
|--------------------|--------------------------------------------------------------------------------------|---------------|
| `API_URL`          | `BASE_HTTP_URL` for the IDP                                                          | `required`    |
| `clientId`         |                                                                                      | `required`    |
| `storagePersister` | An object implementing the SyncStoragePersister interface to manage token responses. | `required`    |

#### Example Usage:

```typescript
import { IdentityProvider } from '@gofiled/react-ui';

const storagePersister = {
  saveTokenResponse: (tokenResponse) => {
    // Implementation to save the token response in storage
  },
  getTokenResponse: () => {
    // Implementation to retrieve the stored token response from storage
  },
  removeTokenResponse: () => {
    // Implementation to remove the stored token response (logout)
  },
};

// Wrap your app with the "IdentityProvider" component
<IdentityProvider IPD_URL="your_IDP_URL" clientId="your_client_id" storagePersister={storagePersister}>
  {/* Your app content */}
</IdentityProvider>;
```

#### useIdentityProvider: Function

This custom hook allows you to access the `identity provider` context. useIdentityProvider is intended to be used in deeply nested structures, where it would become inconvenient to pass the context as a prop.

##### Return

This hook will return all the `useIdentityProvider` return methods and props.

```typescript
const idpMethods = useIdentityProvider();

idpMethods.isLoggedIn; // If there is a user loggedIn
idpMethods.currentUser; // Get Current User
idpMethods.loginWithUserNameAndPassword(); // Login with password
idpMethods.requestOtp(); // request to code OTP
idpMethods.loginWithOtp(); // Login with one time password (OTP)
idpMethods.logout(); // logout func
```

##### native Return

Implement an API service

```typescript
import { HttpClientAbstract } from '@gofiled/react-ui';

class UserService extends HttpClientAbstract {
  // methods....
  constructor() {
    // optional
    const options = {
        onRefreshTokenExpired(){
          // handle refresh token expired maybe you want to logout the user....
        },
        // ....
    }

    super(BASE_API_URL, options);
  // custom handleRequest transformer
  handleRequest(config: InternalAxiosRequestConfig<any>): InternalAxiosRequestConfig<any>;

  // custom handle response transformer
  handleResponse(response: AxiosResponse): any;

  // @override if you want
  // isTokenExpired(error: AxiosError<{ reason?: string }>): boolean;

  }
}
```
